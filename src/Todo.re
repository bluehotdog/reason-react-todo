type todo = {
  text: string,
  isChecked: bool,
};