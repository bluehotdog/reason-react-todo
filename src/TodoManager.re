/* State declaration */
type state = {inputValue: string};

/* Action declaration */
type action =
  | InputChanged(string)
  | AddTodo;

/* Component template declaration.
   Needs to be **after** state and action declarations! */
let component = ReasonReact.reducerComponent("TodoManager");

let inputChange = (event, self) =>
  self.ReasonReact.send(InputChanged(ReactEvent.Form.target(event)##value));

let make = _children => {
  /* spread the other default fields of component here and override a few */
  ...component,
  initialState: () => {inputValue: ""},
  /* State transitions */
  reducer: (action, state) =>
    switch (action) {
    | InputChanged(newVal) =>
      ReasonReact.Update({...state, inputValue: newVal})
    | AddTodo => ReasonReact.NoUpdate
    },
  render: self =>
    <div>
      <input
        type_="text"
        onChange={self.handle(inputChange)}
        value={self.state.inputValue}
      />
      {
        String.length(self.state.inputValue) > 0 ?
          <button type_="button" onClick={_event => self.send(AddTodo)}>
            {ReasonReact.string("Add")}
          </button> :
          ReasonReact.null
      }
      <Todos />
    </div>,
};